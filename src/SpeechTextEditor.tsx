import React, { useEffect } from 'react'
import SpeechRecognition from 'react-speech-recognition';

type Props = {
  transcript: string,
  resetTranscript: () => void,
  browserSupportsSpeechRecognition: boolean,
  startListening: () => void,
  stopListening: () => void,
  abortListening: () => void,
  listening: boolean,
  onTextChanged: (text: string) => void
}

const options = {
  autoStart: false,
  continuous: false
}

const SpeechTextEditor = ({
  transcript,
  resetTranscript,
  browserSupportsSpeechRecognition,
  startListening,
  stopListening,
  listening,
  onTextChanged = () => {}
}: Props) => {
  useEffect(() => {
    onTextChanged(transcript)
  }, [onTextChanged, transcript])

  if (!browserSupportsSpeechRecognition) {
    return null;
  }

  return (
    <div>
      <button onClick={resetTranscript}>Reset</button>
      <button onClick={startListening}>Start</button>
      <button onClick={stopListening}>Stop</button>
      <div>{transcript}</div>

      <div>{listening ? 'true' : 'false'}</div>
    </div>
  );
};

export default SpeechRecognition(options)(SpeechTextEditor);

import React from 'react'
import './App.css';
import SpeechTextEditor from './SpeechTextEditor'

const App = () => {
  return <div className="App">
    <SpeechTextEditor
      onTextChanged={(text: string) => console.log(text)}/>
  </div>
}

export default App;
